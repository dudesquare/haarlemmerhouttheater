<html>
<head>

    <title>Haarlemmerhout Theater</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>


<div class="header">
    <div class="container">
        <div class="logo">
            <img src="images/logo.png">
        </div>
    </div>
    <div class="menu">
        <div class="container">

        </div>
    </div>
</div>

<div class="container">
    <div class="content">
        <div class="image">
            <img src="images/Opzet_slideWelkomHaarlemmerhoutTheater_fotoRamonPhilippo.jpg">
        </div>
        <div class="text">
            <h1>Haarlemmerhout Theater</h1>
            <p>Het Haarlemmerhout Theater heeft helaas haar deuren moeten sluiten. Wij voorzien op de korte termijn geen heropening</p>
        </div>
    </div>
</div>


<div class="footer">
    <div class="container">
        <div class="footer-item">
            <p>Haarlemmerhout Theater</p>
            <p>Van Oldenbarneveltlaan 17</p>
            <p>2012 PA Haarlem</p>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</body>
</html>